package biblioteca;

import biblioteca.model.Carte;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

    public class BigBangTest {

        CartiRepoMock crm = new CartiRepoMock();

        @Test
        public void BigBangTest(){
            adaugaCarte();
            getCartiOrdonateDinAnul();
            cautaCarte();
        }


        public void adaugaCarte(){
            List<String> cuvinteCheie=new ArrayList<String>();
            List<String> referenti=new ArrayList<String>();
            cuvinteCheie.add("Pin");
            referenti.add("Carlo Collodi");
            Carte carte=new Carte();
            carte.setTitlu("Pinocchio");
            carte.setAnAparitie("1950");
            carte.setCuvinteCheie(cuvinteCheie);
            carte.setReferenti(referenti);
            crm.adaugaCarte(carte);
            List<Carte> listaCarti=crm.getCarti();
            assertEquals(carte.toString(), listaCarti.get(listaCarti.size() - 1).toString());
        }


        public void cautaCarte(){
            List<Carte> carti = crm.cautaCarte("Carlo Collodi");
            assertEquals("Carlo Collodi", carti.toString(),"[Pinocchio;Carlo Collodi;1950;Pin]");
            carti.clear();
        }


        public void getCartiOrdonateDinAnul() {
            List<Carte> carti = crm.getCartiOrdonateDinAnul("1950");
            assertEquals("[Pinocchio;Carlo Collodi;1950;Pin]", carti.toString(),"[Pinocchio;Carlo Collodi;1950;Pin]");
            carti.clear();
        }
    }
