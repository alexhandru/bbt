package biblioteca;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class Test {

    BibliotecaCtrl controller;
    CartiRepoInterface crm;

    @Before
    public void setup() throws FileNotFoundException{
        crm = new CartiRepoMock();
        controller = new BibliotecaCtrl(crm);
    }

    @org.junit.Test
    public void testCautaCarte(){
        List<Carte> carti = crm.cautaCarte("Mihai Eminescu");
        assertEquals("Mihai Eminescu", carti.toString(),"[Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint,povesti,povestiri]");
        carti.clear();
        carti=crm.cautaCarte("22");
        assertEquals("22",carti.toString(),"[]");
        carti.clear();
        carti=crm.cautaCarte("Ceaikovski");
        assertEquals("Ceaikovski",carti.toString(),"[]");
    }

    @org.junit.Test
    public void testAdaugaCarte(){
        List<String> cuvinteCheie=new ArrayList<String>();
        List<String> referenti=new ArrayList<String>();
        cuvinteCheie.add("Pin");
        referenti.add("Carlo Collodi");
        Carte carte=new Carte();
        carte.setTitlu("Pinocchio");
        carte.setAnAparitie("1950");
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setReferenti(referenti);
        crm.adaugaCarte(carte);
        List<Carte> listaCarti=crm.getCarti();
        assertEquals(carte.toString(), listaCarti.get(listaCarti.size() - 1).toString());
    }

    @org.junit.Test
    public void testGetCartiOrdonateDinAnul() {
        List<Carte> carti = crm.getCartiOrdonateDinAnul("1948");
        assertEquals("[Dale carnavalului;Caragiale Ion;1948;Litera,caragiale,carnaval, Enigma Otiliei;George Calinescu;1948;Litera,enigma,otilia, Intampinarea crailor;Mateiu Caragiale;1948;Litera,mateiu,crailor]", carti.toString(),"[Dale carnavalului;Caragiale Ion;1948;Litera,caragiale,carnaval, Enigma Otiliei;George Calinescu;1948;Litera,enigma,otilia, Intampinarea crailor;Mateiu Caragiale;1948;Litera,mateiu,crailor]");
        carti.clear();

        carti=crm.getCartiOrdonateDinAnul("2021");
        assertEquals("2021",carti.toString(),"[]");
        carti.clear();
    }

    @org.junit.Test
    public void BigBangTest(){
        testAdaugaCarte();
        testGetCartiOrdonateDinAnul();
        testCautaCarte();
    }


}
