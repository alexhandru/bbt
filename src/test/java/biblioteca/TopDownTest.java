package biblioteca;

import biblioteca.model.Carte;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
public class TopDownTest {
    CartiRepoMock crm=new CartiRepoMock();
    @Test
    public void testTopDown1(){
        testAdaugaCarte();
    }
    @Test
    public void testTopDown2(){
        testAdaugaCarte();
        testCautaCarte();
    }
    @Test
    public void testTopDown3(){
        testAdaugaCarte();
        testCautaCarte();
        testGetCartiOrdonateDinAnul();
    }

    public void testAdaugaCarte(){
        List<String> cuvinteCheie=new ArrayList<String>();
        List<String> referenti=new ArrayList<String>();
        cuvinteCheie.add("Pin");
        referenti.add("Carlo Collodi");
        Carte carte=new Carte();
        carte.setTitlu("Pinocchio");
        carte.setAnAparitie("1950");
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setReferenti(referenti);
        crm.adaugaCarte(carte);
        List<Carte> listaCarti=crm.getCarti();
        assertEquals(carte.toString(), listaCarti.get(listaCarti.size() - 1).toString());
    }

    public void testCautaCarte(){
        List<Carte> carti = crm.cautaCarte("Carlo Collodi");
        assertEquals("Carlo Collodi", carti.toString(),"[Pinocchio;Carlo Collodi;1950;Pin]");
        carti.clear();
        carti=crm.cautaCarte("22");
        assertEquals("22",carti.toString(),"[]");
        carti.clear();
        carti=crm.cautaCarte("Ceaikovski");
        assertEquals("Ceaikovski",carti.toString(),"[]");
    }

    public void testGetCartiOrdonateDinAnul() {
        List<Carte> carti = crm.getCartiOrdonateDinAnul("1950");
        assertEquals("[Pinocchio;Carlo Collodi;1950;Pin]", carti.toString(),"[Pinocchio;Carlo Collodi;1950;Pin]");
        carti.clear();
        carti=crm.getCartiOrdonateDinAnul("2021");
        assertEquals("2021",carti.toString(),"[]");
        carti.clear();
    }
}